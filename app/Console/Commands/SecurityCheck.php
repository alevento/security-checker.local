<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Mail;
use File;
use Validator;

class SecurityCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'security:check
                            {path? : path where find composer.lock, you can use * as jolly character i.e. "/var/www/*/*/", use quotation marks}
                            {--M|mail= : If you want send result to email}'
                            ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check vulnerabilities in packages of composer.lock';


    /**
     * @var Client an istance of GuzzleHttp\Client
     */
    protected $guzzle;

    /**
     * @var string
     */
    protected $testoMessaggioMailHeader = '<!DOCTYPE html>
                                <html>
                                    <head>
                                    <style>
                                        table {
                                            border-collapse: collapse;
                                            padding: 5px;
                                        }

                                        table, td, th {
                                            border: 1px solid black;
                                            padding: 5px;
                                        }
                                        </style>
                                    </head>
                                    <body>
                                    <table border="1" border-collapse="solid" style="width:100%">
                                        <th>Name</th>
                                        <th>Version</th>
                                        <th>Advisor</th>
                                ';

    protected $testoMessaggioMailFooter = '
                                    </table>
                                    </body>
                                </html>
                                ';
    protected $headersTableConsole = ['name', 'version', 'title'];


    /**
     * Create a new command instance.
     *
     * @param Client $objguzzle
     * @return void
     */
    public function __construct(Client $objguzzle)
    {
        $this->guzzle = $objguzzle;
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');
        if($path=='') $path = base_path();

        if (File::isDirectory($path)){
            $path=str_finish($path,'/');
        }

        $lockFiles = $this->findFilesLock($path.'composer.lock');
        $this->info(count($lockFiles).' composer.lock files found.');

        $testoMessaggioMail = $this->testoMessaggioMailHeader;

        $tableVulnerabilities = [];
        $tuttoOk = true;
        $i=0;
        foreach($lockFiles as $fileLock){

            $this->info("Analizing ".($i+1)." di ".count($lockFiles).": $fileLock ...");
            //$tableVulnerabilities[] = $this->getRowSeparator();
            $tableVulnerabilities[] = [
                                    'name' => $fileLock,
                                    'version' => '',
                                    'advisories' => ''
            ];

            $response = $this->getSensiolabVulnerabilties($fileLock);

            if(count($response)>0){
                $this->error("Trovate ".count($response)." vulnerabilita' in $fileLock");
            }
            $testoMessaggioMail .= '<tr><td style="color: '.(count($response)>0 ? 'red; font-weight:bold;' : '').'">'.$fileLock.'</td><td>&nbsp;</td><td>&nbsp;</td></tr>';

            foreach ($response as $key => $vulnerability) {
                $tuttoOk = false;
                $data = [
                    'name' => $key,
                    'version' => $vulnerability['version'],
                    'advisories' => array_values($vulnerability['advisories'])
                ];

                $testoMessaggioMail .= '<tr><td style="color: red;"> '.$data['name'].'</td><td style="color: red;">'.$data['version'].'</td><td style="color: red;">';
                foreach ($data['advisories'] as $key2 => $advisory) {
                    $data2 = [
                        'title' => $advisory['title'],
                        'link' => $advisory['link'],
                        'cve' => $advisory['cve']
                    ];
                    $testoMessaggioMail = $testoMessaggioMail.$data2["title"].'<br/>';

                    $dataTable = [
                        'name' => $data['name'],
                        'version' => $data['version'],
                        'advisories' => $data2["title"]
                    ];

                    $this->addVerboseLog($data['name']." ".$data['version']." ".$data2["title"], true);
                    $tableVulnerabilities[] = $dataTable;
                }

                $testoMessaggioMail .= '</td></tr>';
            }
            //$tableVulnerabilities[] = $this->getRowSeparator();
            $i++;
        }

        //print to console
        $this->table($this->headersTableConsole, $tableVulnerabilities);

        $testoMessaggioMail .= $this->testoMessaggioMailFooter;

        $soggetto='[composer-security-check]: Ok - no vulnerabilities detected.';
        if(!$tuttoOk){
            $soggetto='[composer-security-check]: Alarm - vulnerabilities detected.';
        }

        $this->sendEmail($soggetto, $testoMessaggioMail);
    }


    /**
     * @param $path
     *
     * @return array of composer.lock file
     */
    private function findFilesLock($path)
    {
        $this->info("path: $path . Check composer.lock files...");
        return File::glob($path);
    }

    /**
     *
     * Send Request to sensiolab and return array of sensiolab vulnerabilities.
     * Empty array if here is no vulnerabilities.
     *
     * @param $fileLock path to composer.lock file.
     *
     * @return array
     */
    private function getSensiolabVulnerabilties($fileLock)
    {
        $this->addVerboseLog('Send request to sensiolab: '.$fileLock);
        $request = $this->guzzle->createRequest('POST'
                                                , 'https://security.sensiolabs.org/check_lock'
                                                , [  'headers' => ['Accept' => 'application/json']
                                                    ,'body' => ['lock' => fopen($fileLock, 'r')]
                                                  ]
                                                );

        // get actual response body
        $responseBody = $this->guzzle->send($request)->getBody()->getContents();
        $response = json_decode($responseBody, true);
        return $response;
    }

    /**
     * @param $soggetto
     * @param $testoMessaggioMail
     */
    private function sendEmail($soggetto, $testoMessaggioMail)
    {
        $mail = $this->option('mail');
        if ($mail!='') {
            $validator = Validator::make(['email' => $mail], [
                'email' => 'required|email',
            ]);
            if ($validator->fails()) {
                $this->error('No valid email passed: '.$mail.'. Mail will not be sent.');
                exit;
            }
            $this->addVerboseLog('Send email to '.$mail);
            Mail::send([], [], function ($message) use ($testoMessaggioMail, $mail, $soggetto) {
                $message->to($mail, $mail);
                $message->subject($soggetto);
                $message->setBody($testoMessaggioMail, 'text/html');
            });
        }
    }

    /**
     * @param            $msg
     * @param bool|false $error
     */
    private function addVerboseLog($msg, $error=false)
    {
        $verbose = $this->option('verbose');
        if ($verbose) {
            if($error){
                $this->error($msg);
            }else {
                $this->info($msg);
            }
        }
    }
    /**
     * @return array
     */
    private function getRowSeparator()
    {
        return [
                'name' => '______________________',
                'version' => '',
                'advisories' => '',
              ];
    }
}
